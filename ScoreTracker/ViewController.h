//
//  ViewController.h
//  ScoreTracker
//
//  Created by Mark Farrell on 1/14/15.
//  Copyright (c) 2015 Mark Farrell. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

{
    
    int player1Score;
    int player2Score;
    
    UILabel* player1Lbl;
    UILabel* player2Lbl;
    
    NSString* player1Name;
    NSString* player2Name;
    
    bool active;
    
    UIView* popUpTextFields;
    UITextField* player1NameField;
    UITextField* player2NameField;
    UILabel* player1Title;
    UILabel* player2Title;
    
}

@property (nonatomic)UIStepper* player1Stepper;
@property (nonatomic)UIStepper* player2Stepper;

@end

