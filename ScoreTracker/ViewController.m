//
//  ViewController.m
//  ScoreTracker
//
//  Created by Mark Farrell on 1/14/15.
//  Copyright (c) 2015 Mark Farrell. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UILabel* header = [[UILabel alloc]initWithFrame:(CGRectMake(0, 0, self.view.frame.size.width, 80))];
    header.text = [NSString stringWithFormat:@"Score Tracker"];
    header.backgroundColor = [UIColor blackColor];
    header.font = [UIFont fontWithName:@"Marker Felt" size:30];
    header.textColor = [UIColor lightGrayColor];
    header.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:header];
    
    UIView* view = [[UIView alloc]initWithFrame:(CGRectMake(0, 80, self.view.frame.size.width, self.view.frame.size.height))];
    view.backgroundColor = [UIColor lightGrayColor];
    [self.view addSubview:view];
    player1Score = 0;
    player2Score = 0;
    player1Name = @"Player 1";
    player2Name = @"Player 2";
    active = false;
    
    
    
    player1Lbl = [[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width/4 - 50, 225, 100, 100)];
    player1Lbl.text = [NSString stringWithFormat:@"%d", player1Score];
    player1Lbl.font = [UIFont systemFontOfSize:90];
    [self.view addSubview:player1Lbl];
    
    player1Title = [[UILabel alloc]initWithFrame:CGRectMake(player1Lbl.frame.origin.x, player1Lbl.frame.origin.y - 130, player1Lbl.frame.size.width, 50)];
    player1Title.text = [NSString stringWithFormat:@"Player 1"];
    player1Title.font = [UIFont fontWithName:@"Marker Felt" size:25];
    [view addSubview:player1Title];
    
    _player1Stepper = [[UIStepper alloc]initWithFrame:CGRectMake(player1Lbl.frame.origin.x + 4, player1Lbl.frame.origin.y + player1Lbl.frame.size.height + 40, player1Lbl.frame.size.width, 50)];
    [_player1Stepper addTarget:self action:@selector(player1StepperTouched:) forControlEvents:UIControlEventTouchUpInside];
    _player1Stepper.tintColor = [UIColor blackColor];
    
    [self.view addSubview:_player1Stepper];
    
    player2Lbl = [[UILabel alloc]initWithFrame:(CGRectMake(player1Lbl.frame.origin.x + player1Lbl.frame.size.width + 90 , 225, 100, 100))];
    player2Lbl.text = [NSString stringWithFormat:@"%d", player2Score];
    player2Lbl.font = [UIFont systemFontOfSize:90];
    [self.view addSubview:player2Lbl];
    
    player2Title = [[UILabel alloc]initWithFrame:CGRectMake(player2Lbl.frame.origin.x, player2Lbl.frame.origin.y - 130, player2Lbl.frame.size.width, 50)];
    player2Title.text = [NSString stringWithFormat:@"Player 2"];
    player2Title.font = [UIFont fontWithName:@"Marker Felt" size:25];
    [view addSubview:player2Title];
    
    _player2Stepper = [[UIStepper alloc]initWithFrame:CGRectMake(player2Lbl.frame.origin.x + 4, player2Lbl.frame.origin.y + player2Lbl.frame.size.height + 40, player2Lbl.frame.size.width, 50)];
    [_player2Stepper addTarget:self action:@selector(player2StepperTouched:) forControlEvents:UIControlEventTouchUpInside];
    _player2Stepper.tintColor = [UIColor blackColor];
    
    [self.view addSubview:_player2Stepper];
    
    UIView* footer = [[UIView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height - 150, self.view.frame.size.width, 150)];
    footer.backgroundColor = [UIColor blackColor];
    [self.view addSubview:footer];
    
    UIButton* resetButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [resetButton setTitle:@"Reset" forState:UIControlStateNormal];
    resetButton.frame = CGRectMake(0, 0, 100, 50);
    [resetButton addTarget:self action:@selector(resetButtonTouched:) forControlEvents:UIControlEventTouchUpInside];
    resetButton.center = CGPointMake(footer.frame.size.width/4, self.view.frame.size.height - footer.frame.size.height/2);
    resetButton.tintColor = [UIColor lightGrayColor];
    resetButton.titleLabel.font = [UIFont fontWithName:@"Marker Felt" size:30];
    [resetButton.layer setBorderWidth:2.0];
    [resetButton.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
    [resetButton.layer setCornerRadius:5];
    resetButton.backgroundColor = [UIColor blackColor];
    [self.view addSubview:resetButton];
    
    UIButton* customNameButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [customNameButton setTitle:@"Customize Names" forState:UIControlStateNormal];
    customNameButton.frame = CGRectMake(0, 0, 150, 50);
    [customNameButton addTarget:self action:@selector(customNameButtonTouched:) forControlEvents:UIControlEventTouchUpInside];
    customNameButton.center = CGPointMake(view.frame.size.width/4 + view.frame.size.width/2, self.view.frame.size.height - footer.frame.size.height/2);
    customNameButton.tintColor = [UIColor lightGrayColor];
    customNameButton.titleLabel.font = [UIFont fontWithName:@"Marker Felt" size:20];
    [customNameButton.layer setBorderWidth:2.0];
    [customNameButton.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
    [customNameButton.layer setCornerRadius:5];
    customNameButton.backgroundColor = [UIColor blackColor];
    [self.view addSubview:customNameButton];
    
    
    
    
}

-(void)player1StepperTouched:(UIStepper*)player1Stepper {
    player1Lbl.text = [NSString stringWithFormat:@"%.f", player1Stepper.value];
}

-(void)player2StepperTouched:(UIStepper*)player2Stepper {
    player2Lbl.text = [NSString stringWithFormat:@"%.f", player2Stepper.value];
}

-(void)resetButtonTouched:(UIButton*)resetButton {
    _player1Stepper.value = 0;
    _player2Stepper.value = 0;
    player1Lbl.text = [NSString stringWithFormat:@"0"];
    player2Lbl.text = [NSString stringWithFormat:@"0"];
    player1Title.text = [NSString stringWithFormat:@"Player 1"];
    player2Title.text = [NSString stringWithFormat:@"Player 2"];
}

-(void)customNameButtonTouched:(UIButton*)customNameButton{
    popUpTextFields = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width /4 - 25, 100, self.view.frame.size.width/2 + 50, 150)];
    [popUpTextFields.layer setBorderWidth: 3];
    [popUpTextFields.layer setShadowColor:[[UIColor redColor] CGColor]];
    [popUpTextFields.layer setShadowOffset:CGSizeMake(3, 3)];
    [popUpTextFields.layer setCornerRadius:5];
    popUpTextFields.backgroundColor = [UIColor darkGrayColor];
    [self.view addSubview:popUpTextFields];
    
    
    player1NameField = [[UITextField alloc]initWithFrame:CGRectMake(0, 0, 195, 30)];
    player1NameField.text = [NSString stringWithFormat:@"Player 1 name"];
    player1NameField.center = CGPointMake(popUpTextFields.frame.size.width / 2, 30);
    [player1NameField.layer setBorderWidth:2];
    [player1NameField.layer setCornerRadius:5];
    player1NameField.backgroundColor = [UIColor whiteColor];
    player1NameField.textAlignment = NSTextAlignmentCenter;
    [popUpTextFields addSubview:player1NameField];
    
    player2NameField = [[UITextField alloc]initWithFrame:CGRectMake(0, 0, 195, 30)];
    player2NameField.center = CGPointMake(popUpTextFields.frame.size.width/2, 75);
    player2NameField.text = [NSString stringWithFormat:@"Player 2 name"];
    [player2NameField.layer setBorderWidth:2];
    [player2NameField.layer setCornerRadius:5];
    player2NameField.backgroundColor = [UIColor whiteColor];
    player2NameField.textAlignment = NSTextAlignmentCenter;
    [popUpTextFields addSubview:player2NameField];
    
    UIButton* submitChangeButton = [UIButton buttonWithType:UIButtonTypeSystem];
    submitChangeButton.frame = CGRectMake(0, 0, 60, 30);
    submitChangeButton.center = CGPointMake(popUpTextFields.frame.size.width/2 + popUpTextFields.frame.size.width/4 + 10, 115);
    [submitChangeButton setTitle:@"Submit" forState: UIControlStateNormal];
    [submitChangeButton.layer setBorderWidth:2];
    [submitChangeButton.layer setCornerRadius:5];
    submitChangeButton.tintColor = [UIColor blackColor];
    submitChangeButton.backgroundColor = [UIColor lightGrayColor];
    [submitChangeButton addTarget:self action:@selector(submitChangeButtonTouched:) forControlEvents:UIControlEventTouchUpInside];
    [popUpTextFields addSubview:submitChangeButton];
    
    
    
}

-(void)submitChangeButtonTouched:(UIButton*)submitChangeButton{
    player1Title.text = player1NameField.text;
    player2Title.text = player2NameField.text;
    [popUpTextFields removeFromSuperview];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

@end
